package org.example.exception;


public class FlatNotFoundException extends RuntimeException {

    public FlatNotFoundException() {
    }

    public FlatNotFoundException(String alert) {
        super(alert);
    }

    public FlatNotFoundException(String alert, Throwable reason) {
        super(alert, reason);
    }

    public FlatNotFoundException(Throwable reason) {
        super(reason);
    }

    protected FlatNotFoundException(String alert, Throwable reason, boolean enableSuppression, boolean writableStackTrace) {
        super(alert, reason, enableSuppression, writableStackTrace);
    }

}
