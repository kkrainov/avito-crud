package org.example.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor

public class FlatModel {
    private long id;

    private int flatFloorValue;
    private int totalFloorsInHouseValue;

    private int roomsValue;

    private boolean isStudio;
    private boolean isFreePlanned;

    private int wholeSquareValue;

    private boolean isWithBalcony;
    private boolean isWithLoggia;

    private int flatCostValue;

    private boolean moreInfo;
    private boolean location;

    private boolean notActive;

    public FlatModel(int flatFloorValue, int totalFloorsInHouseValue, int roomsValue, boolean isStudio, boolean isFreePlanned,
                     int wholeSquareValue, boolean isWithBalcony, boolean isWithLoggia, int flatCostValue,
                     boolean moreInfo, boolean location, boolean notActive) {
        this(0, flatFloorValue, totalFloorsInHouseValue, roomsValue, isStudio,
                isFreePlanned, wholeSquareValue, isWithBalcony,
                isWithLoggia, flatCostValue, moreInfo, location, false);
    }

}
