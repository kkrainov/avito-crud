package org.example.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.data.FlatModel;
import org.example.dto.RequestModel;
import org.example.exception.FlatNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class RealtyManager {

    private static final int SEARCH_SIZE_LIMIT_VALUE = 7;
    private static final int MAX_ROOMS_IN_FLAT_VALUE = 99;

    private final List<FlatModel> flats = new ArrayList<>(100);

    private long nextObjectID = 1;

    public List<FlatModel> grabAll() {
        log.debug("grabs all flats, value: {}", flats.size());

        return new ArrayList<>(flats);
    }

    public FlatModel grabById(final long id) {
        for (final FlatModel flat : flats) {
            if (flat.getId() == id) {
                log.debug("get specific flat by id: {}", id);

                return flat;
            }
        }
        throw new FlatNotFoundException("flat with id " + id + " not found");
    }

    public FlatModel createFlat(final FlatModel flat) {
        log.debug("create flat, nextId: {}, has location: {}", nextObjectID, flat.isLocation());

        flat.setId(nextObjectID);
        nextObjectID++;

        flats.add(flat);
        return flat;
    }

    public FlatModel updateFlat(final FlatModel flat) {
        log.debug("update flat, id: {}", flat.getId());

        final int flatIndex = getIndexById(flat.getId());

        if (flatIndex == -1) {
            throw new FlatNotFoundException("can't update flat with id " + flat.getId() + " (not found)");
        }

        flats.set(flatIndex, flat);

        log.debug("flat updated, id: {}", flat.getId());

        return flat;
    }

    public boolean removeFlatById(final long id) {
        log.debug("deleting flat by id: {}", id);

        return flats.removeIf(e -> e.getId() == id);
    }

    public int countFlats() {
        return flats.size();
    }


    public List<FlatModel> searching(final RequestModel searchRequest) {
        final List<FlatModel> searchedObjects = new ArrayList<>(SEARCH_SIZE_LIMIT_VALUE);

        log.debug("search by criteria in a query: {}", searchRequest);

        for (final FlatModel flat : flats) {
            if (matching(flat, searchRequest)) {
                searchedObjects.add(flat);
                if (searchedObjects.size() >= SEARCH_SIZE_LIMIT_VALUE) {
                    return searchedObjects;
                }
            }
        }
        return searchedObjects;
    }

    public int getIndexById(final long id) {
        for (int i = 0; i < flats.size(); i++) {
            final FlatModel flatObject = flats.get(i);

            if (flatObject.getId() == id) {
                return i;
            }
        }
        return -1;
    }

    public boolean matching(final FlatModel flat, final RequestModel searchRequest) {

        if (flat.getFlatFloorValue() < searchRequest.getFlatFloorMinValue()) {
            return false;
        }

        if (flat.getFlatFloorValue() > searchRequest.getFlatFloorMaxValue()) {
            return false;
        }

        if (flat.getFlatFloorValue() == 1 && searchRequest.isNotFirstFloorValue()) {
            return false;
        }

        if (flat.getFlatFloorValue() == flat.getTotalFloorsInHouseValue() && searchRequest.isNotLastFloorValue()) {
            return false;
        }

        if (flat.getTotalFloorsInHouseValue() < searchRequest.getFloorsInHouseMinValue()) {
            return false;
        }

        if (flat.getTotalFloorsInHouseValue() > searchRequest.getFloorsInHouseMaxValue()) {
            return false;
        }

        if (searchRequest.getRoomsMaxValue() >= searchRequest.MAX_ROOMS_NUMBER_SEARCH) {
            searchRequest.setRoomsMaxValue(MAX_ROOMS_IN_FLAT_VALUE);
        }
        if (flat.getRoomsValue() < searchRequest.getRoomsMinValue()) {
            return false;
        }
        if (flat.getRoomsValue() > searchRequest.getRoomsMaxValue()) {
            return false;
        }

        if (flat.isStudio() != searchRequest.isStudioValue()) {
            return false;
        }

        if (flat.isFreePlanned() != searchRequest.isFreePlannedValue()) {
            return false;
        }

        if (flat.getWholeSquareValue() < searchRequest.getWholeSquareMinValue()) {
            return false;
        }
        if (flat.getWholeSquareValue() > searchRequest.getWholeSquareMaxValue()) {
            return false;
        }

        if (flat.isWithBalcony() != searchRequest.isBalconyValue()) {
            return false;
        }

        if (flat.isWithLoggia() != searchRequest.isLoggiaValue()) {
            return false;
        }

        if (flat.getFlatCostValue() < searchRequest.getMinCostValue()) {
            return false;
        }
        if (flat.getFlatCostValue() > searchRequest.getMaxCostValue()) {
            return false;
        }

        if (flat.isMoreInfo() != searchRequest.isHasMoreInfoValue()) {
            return false;
        }

        if (flat.isLocation() != searchRequest.isHasLocationValue()) {
            return false;
        }

        return true;
    }

}

